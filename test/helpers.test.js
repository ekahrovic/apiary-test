const { expect } = require('chai');

const {
  isURL,
  toRawHeader
} = require('../src/helpers');


// describe('function isUrl()', () => {
//   const validUrls = [
//     'http://✪df.ws/123',
//     'http://userid:password@example.com:8080',
//     'http://userid:password@example.com:8080/',
//     'http://userid@example.com',
//     'http://userid@example.com/',
//     'http://userid@example.com:8080',
//     'http://userid@example.com:8080/',
//     'http://userid:password@example.com',
//     'http://userid:password@example.com/',
//     'http://142.42.1.1/',
//     'http://142.42.1.1:8080/',
//     'http://➡.ws/䨹',
//     'http://⌘.ws',
//     'http://⌘.ws/',
//     'http://foo.com/blah_(wikipedia)#cite-1',
//     'http://foo.com/blah_(wikipedia)_blah#cite-1',
//     'http://foo.com/unicode_(✪)_in_parens',
//     'http://foo.com/(something)?after=parens',
//     'http://☺.damowmow.com/',
//     'http://code.google.com/events/#&product=browser',
//     'http://j.mp',
//     'ftp://foo.bar/baz',
//     'http://foo.bar/?q=Test%20URL-encoded%20stuff',
//     'http://مثال.إختبار',
//     'http://例子.测试'
//   ];

//   const invvalidUrls = [
//     'http://',
//     'http://.',
//     'http://..',
//     'http://../',
//     'http://?',
//     'http://??',
//     'http://??/',
//     'http://#',
//     'http://##',
//     'http://##/',
//     'http://foo.bar?q=Spaces should be encoded',
//     '//',
//     '//a',
//     '///a',
//     '///',
//     'http:///a',
//     'foo.com',
//     'rdar://1234',
//     'h://test',
//     'http:// shouldfail.com',
//     ':// should fail',
//     'http://foo.bar/foo(bar)baz quux',
//     'ftps://foo.bar/',
//     'http://-error-.invalid/',
//     'http://a.b--c.de/',
//     'http://-a.b.co',
//     'http://a.b-.co',
//     'http://0.0.0.0',
//     'http://10.1.1.0',
//     'http://10.1.1.255',
//     'http://224.1.1.1',
//     'http://1.1.1.1.1',
//     'http://123.123.123',
//     'http://3628126748',
//     'http://.www.foo.bar/',
//     'http://www.foo.bar./',
//     'http://.www.foo.bar./',
//     'http://10.1.1.1',
//     'http://10.1.1.254'
//   ];

//   validUrls.forEach((url) => {
//     it('should return true for valid url', () => {
//       expect(isURL(url), `url: '${url}'`).to.be.true;
//     });
//   });
//   validUrls.forEach((url) => {
//     it('should return false for invalid url', () => {
//       expect(isURL(url), `url: '${url}'`).to.be.false;
//     });
//   });
// });

describe('function objToStr()', () => {
  it('converts Response object to string', () => {
    const obj = {
      connection: 'close',
      server: 'gunicorn/19.9.0',
      date: 'Sun, 09 Dec 2018 21:16:42 GMT',
      'content-type': 'application/json',
      'content-length': '381',
      'access-control-allow-origin': '*',
      'access-control-allow-credentials': 'true',
      via: '1.1 vegur'
    };

    const str = 'Connection: close\n'
      + 'Server: gunicorn/19.9.0\n'
      + 'Date: Sun, 09 Dec 2018 21:16:42 GMT\n'
      + 'Content-Type: application/json\n'
      + 'Content-Length: 381\n'
      + 'Access-Control-Allow-Origin: *\n'
      + 'Access-Control-Allow-Credentials: true\n'
      + 'Via: 1.1 vegur\n';

    expect(toRawHeader(obj)).to.equal(str);
  });
});
