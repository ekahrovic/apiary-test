const { expect } = require('chai');
const { exec } = require('child_process');

describe('post CLI', () => {
  it('Prints response status, headers and body (and nothing more) if url and input data аrе valid', (done) => {
    const data = '{"data": 42}';
    const url = 'https://httpbin.org/post';

    const cmd = exec(`echo ${data} | post ${url}`, (err, stdout, stderr) => {
      done();
    });

    cmd.stdout.on('data', (d) => {
      expect(parseInt(d, 10)).to.be.within(100, 512);
      expect(d).to.include('Content-Type: application/json');
      expect(d).to.include('Content-Type: application/json');
      expect(d).to.include('Server: gunicorn/19.9.0');
      expect(d).to.include('Connection:');
      expect(d).to.include('Content-Length:');
    });

    // cmd.stderr.on('data', (d) => {
    //   // console.log('\n\n\nERRRRRROR\n\n\n');
    // });

    // cmd.on('close', (code) => {
    //   // console.log(output);
    // });
  });
});
