#!/usr/bin/env node
const program = require('commander');
const fs = require('fs');
const path = require('path');
const post = require('./post');
const {
  toRawHeader,
  openInBrowser
} = require('./helpers');

const currentPath = path.resolve();
const lastHTMLResponsePath = path.join(currentPath, 'src', 'lastResponse.js');
const htmlPagePath = path.join(currentPath, 'src', 'index.html');

process.stdin.resume();
process.stdin.once('data', (data) => {
  const input = data.toString().trim();

  if (input.length < 1) {
    console.log('No data to be sent');
    process.exit(1);
  }

  program
    .version('0.0.1')
    .description('Send Generic HTTP Post Request')
    .option('-h, --html', 'Open result in browser window')
    .arguments('<url>')
    .action(async (url) => {
      try {
        await post(url, input)
          .then((res) => {
            const response = res.reduce((acc, cur, i) => {
              // TODO: Refactor this shame!
              if (i === 0) { acc.status = cur; } else if (i === 1) { acc.headers = toRawHeader(cur); } else if (i === 2) { acc.body = cur; }
              return acc;
            }, {});

            if (program.html) {
              fs.writeFile(lastHTMLResponsePath,
                `const json = ${JSON.stringify({ url, data: input, response })}`,
                (error) => {
                  if (error) console.error(error);
                }, openInBrowser(htmlPagePath));
            } else {
              process.stdout.write(`${response.status}
              \n${response.headers}
              \n${response.body}`);
            }
          })
          .catch(err => console.log(err));
      } catch (e) {
        console.error(e);
      }
    })
    .parse(process.argv);
});

process.stdout.on('error', (err) => {
  if (err.code === 'EPIPE') return process.exit();
  process.emit('error', err);
});
