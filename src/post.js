const { isURL } = require('./helpers');

const post = function (myUrl, data) {
  if (!isURL(myUrl)) {
    throw new Error('URL not valid');
  }

  return new Promise((resolve, reject) => {
    // eslint-disable-next-line global-require
    const http = myUrl.startsWith('https') ? require('https') : require('http');

    const url = new URL(myUrl);

    const options = {
      hostname: url.hostname,
      path: url.pathname,
      port: url.port,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'User-Agent': 'post/1.0 (+https://apiary.io)'
      }
    };

    const request = http.request(options, (res) => {
      // if (res.statusCode < 200 || res.statusCode > 299) {
      // reject(new Error(`Failed to load page, status code: ${res.statusCode}`));
      // }
      const body = [];
      body.push(res.statusCode);
      body.push(res.headers);
      res.on('data', chunk => body.push(chunk.toString()));
      res.on('end', () => resolve(body));
    });

    request.on('error', err => reject(err));
    request.write(data);
    request.end();
  });
};

module.exports = post;
