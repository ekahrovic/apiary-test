const defaultBrowser = require('default-browser');
const open = require('open');


/**
 * Check if url is valid.
 * @param  {string} str
 * @returns {boolean} Wheather the string is url or not
 */
function isURL(str) {
  // FIXME: incorrect and totally crazy... and everything else...
  const res = str.match(/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.​\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[​6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1​,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00​a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u​00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i);
  if (res === null) return false;
  return true;
}

/**
 * Convert Response object to string.
 * @param  {object} obj
 * @returns {string} String representation of an object
 */
function objToStr(obj) {
  let str = '';
  Object.keys(obj).forEach((k) => {
    if (typeof obj[k] === 'object') {
      return objToStr(obj[k]);
    }
    str += `${k}: ${obj[k]}\n`;
  });
  return str;
}

/**
 * Convert HTTP response headers object to raw headers
 * @param  {object} Object HTTP headers
 * @returns {string} Raw HTTP headers
 */
function toRawHeader(headers) {
  const h = {};

  Object.keys(headers).forEach((oldkey) => {
    const newkey = oldkey.replace(/((?:^|-)[a-z])/g, val => val.toUpperCase());
    h[newkey] = headers[oldkey];
  });
  return objToStr(h);
}

/**
 * Open local HTML page in default browser.
 * @param  {string} local html page path
 */
function openInBrowser(htmlPagePath) {
  defaultBrowser().then((browser) => {
    switch (browser.name) {
      case 'Safari':
      case 'Firefox':
        open(htmlPagePath, browser.name);
        break;
      case 'Chrome':
        open(htmlPagePath, 'google chrome');
        break;
      default:
        console.log('To see response in browser you must have one of the browsers: '
          + 'Safari, Google Chrome or Firefox');
    }
  });
}


module.exports = {
  isURL,
  toRawHeader,
  objToStr,
  openInBrowser
};
