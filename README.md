## CLI Post generic HTTP request

## Installation
`npm link`


## Usage examples

`echo '{"data": 7}' | post https://httpbin.org/post`

`echo '{"data": 7}' | post https://httpbin.org/post --html`

